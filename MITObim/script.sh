## interleave paired end files
nohup python2 MITObim/misc_scripts/interleave-fastqgz-MITOBIM.py /media/bigvol/peguerin/170721_SNK268_B_L006_JFP-1_R1.fastq.gz \
/media/bigvol/peguerin/170721_SNK268_B_L006_JFP-1_R2.fastq.gz > /media/bigvol/peguerin/mullus_interleaved_150bp_350insert.fastq &
## mitobim run
sudo docker stop mitobim.simg
sudo docker rm mitobim.simg
WORKING_DIR=/media/bigvol/peguerin/
sudo docker run -d -i -t --name mitobim.simg -w /home/data -v $WORKING_DIR/:/home/data chrishah/mitobim /bin/bash 
## mitobim exec
sudo docker exec -i -t -d mitobim.simg /home/src/scripts/MITObim.pl -sample mulluspool -ref mullus_dloop -readpool mullus_interleaved_150bp_350insert.fastq --quick mullus_dloop.fasta -end 100 --clean &> log

