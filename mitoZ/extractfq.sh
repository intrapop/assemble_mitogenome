## softwares/scripts
EXTRACT_FQ_SEQ=`echo "python3 //media/bigvol/peguerin/extractfq/extractfq/extractfq.py"`
## data files
#RAWFQGZ1="/media/bigvol/peguerin/170721_SNK268_B_L006_JFP-1_R1.fastq.gz"
#RAWFQGZ2="/media/bigvol/peguerin/170721_SNK268_B_L006_JFP-1_R2.fastq.gz"
RAWFQGZ1=$1
RAWFQGZ2=$2
FOLDERRAW=$3
mkdir $FOLDERRAW
##extract 5 Go
$EXTRACT_FQ_SEQ -fq1 $RAWFQGZ1 -fq2 $RAWFQGZ2 -outfq1 $FOLDERRAW"/raw.1.fq" -outfq2 $FOLDERRAW"/raw.2.fq" -size_required 5
