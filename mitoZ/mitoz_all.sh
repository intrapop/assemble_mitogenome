## softwares/scripts
MITOZ="/media/bigvol/peguerin/MitoZ.simg"
## data files
RAWFQGZ1=$1"/raw.1.fq"
RAWFQGZ2=$1"/raw.2.fq"
OUTPREFIX=$2
## run mitoz
$MITOZ  all2 --genetic_code 2 --clade Chordata --outprefix $OUTPREFIX \
--thread_number 64 \
--fastq1 $RAWFQGZ1 \
--fastq2 $RAWFQGZ2 \
--fastq_read_length 150 \
--insert_size 350  \
--run_mode 2 \
--filter_taxa_method 1 \
--requiring_taxa 'Chordata'
