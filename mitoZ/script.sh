## softwares/scripts
MITOZ="/home/pguerin/working/intrapop/projets/assemble_mitogenome/MitoZ.simg"
EXTRACT_FQ_SEQ=`echo "python3 /home/pguerin/working/intrapop/projets/assemble_mitogenome/extractfq/extractfq/extractfq.py"`
## data files
#### mullus surmuletus
RAWFQGZ1="/media/bigvol/peguerin/170721_SNK268_B_L006_JFP-1_R1.fastq.gz"
RAWFQGZ2="/media/bigvol/peguerin/170721_SNK268_B_L006_JFP-1_R2.fastq.gz"
FOLDERRAW="/media/bigvol/peguerin/raw_mullus/"
OUTPREFIX="mullus_mitoz"
#### diplodus sargus
RAWFQGZ1="goodlength_goodqual_170106_SNK268_A_L006_GWM-869_R1.fastq.gz"
RAWFQGZ2="goodlength_goodqual_170106_SNK268_A_L006_GWM-869_R2.fastq.gz"
FOLDERRAW="/media/bigvol/peguerin/raw_diplodus/"
OUTPREFIX="diplodus_mitoz"
#### serranus cabrilla
RAWFQGZ1="180802_NB501473_A_L1-4_ANIZ-1_R1.RD30.NotEmpty.LinkerTrimmed-50bp-PR.fastq.gz"
RAWFQGZ2="180802_NB501473_A_L1-4_ANIZ-1_R2.RD30.NotEmpty.LinkerTrimmed-50bp-PR.fastq.gz"
FOLDERRAW="/media/bigvol/peguerin/raw_serran/"
OUTPREFIX="serran_mitoz"

## extractfq
nohup bash extractfq.sh $RAWFQGZ1 $RAWFQGZ2 $FOLDERRAW &
## mitoz
nohup bash mitoz_all.sh $FOLDERRAW $OUTPREFIX &



