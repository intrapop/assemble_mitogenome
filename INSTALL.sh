###############################################################################
## install docker
sudo apt-get update -y
sudo apt-get install \
    apt-transport-https \
    ca-certificates \
    curl \
    software-properties-common
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable"
sudo apt-get update
sudo apt-get install docker-ce
sudo docker run hello-world

###############################################################################
## MitoZ
## mitoz singularuty img
sudo singularity pull --name MitoZ.simg shub://linzhi2013/MitoZ:v2.3
## extract fastq seq python3 script
git clone https://github.com/linzhi2013/extractfq.git

###############################################################################
## MITObim
## MITObim docker img

sudo docker pull chrishah/mitobim

git clone https://github.com/chrishah/MITObim.git

###############################################################################
## Norgal

git clone https://github.com/kosaidtu/norgal.git