# assemble_mitogenome

Collections of  mitogenome assembly methods from whole genome sequencing data

# Installation

These scripts have been ran and tested under `LINUX kernel version 4.15.0-50-generic`

You will need to download `illumina` `paired end` `fastq.gz` files from Next generation sequencing. 

In our case study, we used whole genome sequencing data of one individual for 3 different fishes species

* _Diplodus sargus_
* _Mullus surmuletus_
* _Serranus Cabrilla_

Data were provided by [FASTERIS company](https://www.fasteris.com/dna/) sequencing services.

We tried here 3 different methods of mitogenome assembly. 
Please follow instructions into the `bash` script :

* [INSTALL.sh](INSTALL.sh)

# Mitochondrion assembly

## MitoZ

### About MitoZ

MitoZ is a Python3-based toolkit which aims to automatically filter pair-end raw data (fastq files), assemble genome, search for mitogenome sequences from the genome assembly result, annotate mitogenome (genbank file as result), and mitogenome visualization. MitoZ is available from https://github.com/linzhi2013/MitoZ.

### run MitoZ

```
bash mitoZ/script.sh
```


## MITObim


### About MITObim

The MITObim procedure (mitochondrial baiting and iterative mapping) represents a highly efficient approach to assembling novel mitochondrial genomes of non-model organisms directly from total genomic DNA derived NGS reads. Labor intensive long-range PCR steps prior to sequencing are no longer required. MITObim is capable of reconstructing mitochondrial genomes without the need of a reference genome of the targeted species by relying solely on (a) mitochondrial genome information of more distantly related taxa or (b) short mitochondrial barcoding sequences (seeds), such as the commonly used cytochrome-oxidase subunit 1 (COI), as a starting reference. MitoBIM is available from https://github.com/chrishah/MITObim.

### run MITObim

```
bash MITObim/script.sh
```

## Norgal

### About Norgal

Norgal takes NGS sequencing reads as input (currently only Illumina paired end reads are supported) and tries to assemble the mitochondrial genome using kmer frequencies. Norgal is available from https://bitbucket.org/kosaidtu/norgal/src/master/.

### run Norgal

```
bash norgal/script.sh
```